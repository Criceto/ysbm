/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50545
Source Host           : localhost:3306
Source Database       : ysbm

Target Server Type    : MYSQL
Target Server Version : 50545
File Encoding         : 65001

Date: 2016-09-09 02:08:57
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for mail_templates
-- ----------------------------
DROP TABLE IF EXISTS `mail_templates`;
CREATE TABLE `mail_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `text` text,
  `updated_at` int(11) DEFAULT NULL,
  `status` smallint(1) DEFAULT '0',
  `sort` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mail_templates
-- ----------------------------
INSERT INTO `mail_templates` VALUES ('1', 'Отправка нового пароля с формы \"Забыли пароль\"', 'Новый пароль для входа на сайт {{site}}', '<p>Здравствуйте! Вы воспользовались формой \"Забыли пароль\" на сайте {{site}}. Для входа на сайт можете воспользоваться следующими данными:</p>\r\n                        <p>&nbsp;</p>\r\n                        <p>E-mail: {{email}}.</p>\r\n                        <p>Пароль: {{password}}.</p>\r\n                        <p>&nbsp;</p>\r\n                        <p>С Ув. Администрация сайта {{site}}.</p>', null, '1', '0');

-- ----------------------------
-- Table structure for migration
-- ----------------------------
DROP TABLE IF EXISTS `migration`;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of migration
-- ----------------------------
INSERT INTO `migration` VALUES ('m000000_000000_base', '1473367686');
INSERT INTO `migration` VALUES ('m130524_201442_init', '1473367688');
INSERT INTO `migration` VALUES ('m160729_125935_add_table_user_roles', '1473367688');
INSERT INTO `migration` VALUES ('m160802_104019_change_username_in_user', '1473367688');
INSERT INTO `migration` VALUES ('m160803_093548_first_user', '1473367688');
INSERT INTO `migration` VALUES ('m160804_104947_add_mail_templates_table', '1473367688');
INSERT INTO `migration` VALUES ('m160908_201803_add_table_tests', '1473367688');
INSERT INTO `migration` VALUES ('m160908_202735_questions', '1473367688');
INSERT INTO `migration` VALUES ('m160908_204244_test_results', '1473367688');
INSERT INTO `migration` VALUES ('m160908_223929_add_admin_user', '1473374566');

-- ----------------------------
-- Table structure for questions
-- ----------------------------
DROP TABLE IF EXISTS `questions`;
CREATE TABLE `questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `test_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `sort` int(11) DEFAULT '0',
  `status` smallint(1) DEFAULT '1',
  `answers` text,
  `correct_answer` int(11) DEFAULT '0',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `question_to_test` (`test_id`),
  CONSTRAINT `question_to_test` FOREIGN KEY (`test_id`) REFERENCES `tests` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of questions
-- ----------------------------
INSERT INTO `questions` VALUES ('1', '1', 'Вопрос №1', '0', '1', '{\"1\":\"\\u041f\\u0440\\u0430\\u0432\\u0438\\u043b\\u044c\\u043d\\u044b\\u0439 \\u043e\\u0442\\u0432\\u0435\\u0442\",\"2\":\"\\u041e\\u0442\\u0432\\u0435\\u0442 \\u21162\",\"3\":\"\\u041e\\u0442\\u0432\\u0435\\u0442 \\u21163\",\"4\":\"\\u041e\\u0442\\u0432\\u0435\\u0442 \\u21164\",\"5\":\"\\u041e\\u0442\\u0432\\u0435\\u0442 \\u21165\"}', '1', '1473367688', '1473367688');
INSERT INTO `questions` VALUES ('2', '2', 'Вопрос №1', '0', '1', '{\"1\":\"\\u041f\\u0440\\u0430\\u0432\\u0438\\u043b\\u044c\\u043d\\u044b\\u0439 \\u043e\\u0442\\u0432\\u0435\\u0442\",\"2\":\"\\u041e\\u0442\\u0432\\u0435\\u0442 \\u21162\",\"3\":\"\\u041e\\u0442\\u0432\\u0435\\u0442 \\u21163\",\"4\":\"\\u041e\\u0442\\u0432\\u0435\\u0442 \\u21164\",\"5\":\"\\u041e\\u0442\\u0432\\u0435\\u0442 \\u21165\"}', '1', '1473367688', '1473367688');
INSERT INTO `questions` VALUES ('3', '1', 'Вопрос №2', '0', '1', '{\"1\":\"\\u041e\\u0442\\u0432\\u0435\\u0442 \\u21161\",\"2\":\"\\u041f\\u0440\\u0430\\u0432\\u0438\\u043b\\u044c\\u043d\\u044b\\u0439 \\u043e\\u0442\\u0432\\u0435\\u0442\",\"3\":\"\\u041e\\u0442\\u0432\\u0435\\u0442 \\u21163\",\"4\":\"\\u041e\\u0442\\u0432\\u0435\\u0442 \\u21164\",\"5\":\"\\u041e\\u0442\\u0432\\u0435\\u0442 \\u21165\"}', '2', '1473367688', '1473367688');
INSERT INTO `questions` VALUES ('4', '2', 'Вопрос №2', '0', '1', '{\"1\":\"\\u041e\\u0442\\u0432\\u0435\\u0442 \\u21161\",\"2\":\"\\u041f\\u0440\\u0430\\u0432\\u0438\\u043b\\u044c\\u043d\\u044b\\u0439 \\u043e\\u0442\\u0432\\u0435\\u0442\",\"3\":\"\\u041e\\u0442\\u0432\\u0435\\u0442 \\u21163\",\"4\":\"\\u041e\\u0442\\u0432\\u0435\\u0442 \\u21164\",\"5\":\"\\u041e\\u0442\\u0432\\u0435\\u0442 \\u21165\"}', '2', '1473367688', '1473367688');
INSERT INTO `questions` VALUES ('5', '1', 'Вопрос №3', '0', '1', '{\"1\":\"\\u041e\\u0442\\u0432\\u0435\\u0442 \\u21161\",\"2\":\"\\u041e\\u0442\\u0432\\u0435\\u0442 \\u21162\",\"3\":\"\\u041f\\u0440\\u0430\\u0432\\u0438\\u043b\\u044c\\u043d\\u044b\\u0439 \\u043e\\u0442\\u0432\\u0435\\u0442\",\"4\":\"\\u041e\\u0442\\u0432\\u0435\\u0442 \\u21164\",\"5\":\"\\u041e\\u0442\\u0432\\u0435\\u0442 \\u21165\"}', '3', '1473367688', '1473367688');
INSERT INTO `questions` VALUES ('6', '2', 'Вопрос №3', '0', '1', '{\"1\":\"\\u041e\\u0442\\u0432\\u0435\\u0442 \\u21161\",\"2\":\"\\u041e\\u0442\\u0432\\u0435\\u0442 \\u21162\",\"3\":\"\\u041f\\u0440\\u0430\\u0432\\u0438\\u043b\\u044c\\u043d\\u044b\\u0439 \\u043e\\u0442\\u0432\\u0435\\u0442\",\"4\":\"\\u041e\\u0442\\u0432\\u0435\\u0442 \\u21164\",\"5\":\"\\u041e\\u0442\\u0432\\u0435\\u0442 \\u21165\"}', '3', '1473367688', '1473367688');
INSERT INTO `questions` VALUES ('7', '1', 'Вопрос №4', '0', '1', '{\"1\":\"\\u041e\\u0442\\u0432\\u0435\\u0442 \\u21161\",\"2\":\"\\u041e\\u0442\\u0432\\u0435\\u0442 \\u21162\",\"3\":\"\\u041e\\u0442\\u0432\\u0435\\u0442 \\u21163\",\"4\":\"\\u041f\\u0440\\u0430\\u0432\\u0438\\u043b\\u044c\\u043d\\u044b\\u0439 \\u043e\\u0442\\u0432\\u0435\\u0442\",\"5\":\"\\u041e\\u0442\\u0432\\u0435\\u0442 \\u21165\"}', '4', '1473367688', '1473367688');
INSERT INTO `questions` VALUES ('8', '2', 'Вопрос №4', '0', '1', '{\"1\":\"\\u041e\\u0442\\u0432\\u0435\\u0442 \\u21161\",\"2\":\"\\u041e\\u0442\\u0432\\u0435\\u0442 \\u21162\",\"3\":\"\\u041e\\u0442\\u0432\\u0435\\u0442 \\u21163\",\"4\":\"\\u041f\\u0440\\u0430\\u0432\\u0438\\u043b\\u044c\\u043d\\u044b\\u0439 \\u043e\\u0442\\u0432\\u0435\\u0442\",\"5\":\"\\u041e\\u0442\\u0432\\u0435\\u0442 \\u21165\"}', '4', '1473367688', '1473367688');
INSERT INTO `questions` VALUES ('9', '1', 'Вопрос №5', '0', '1', '{\"1\":\"\\u041e\\u0442\\u0432\\u0435\\u0442 \\u21161\",\"2\":\"\\u041e\\u0442\\u0432\\u0435\\u0442 \\u21162\",\"3\":\"\\u041e\\u0442\\u0432\\u0435\\u0442 \\u21163\",\"4\":\"\\u041e\\u0442\\u0432\\u0435\\u0442 \\u21164\",\"5\":\"\\u041f\\u0440\\u0430\\u0432\\u0438\\u043b\\u044c\\u043d\\u044b\\u0439 \\u043e\\u0442\\u0432\\u0435\\u0442\"}', '5', '1473367688', '1473367688');
INSERT INTO `questions` VALUES ('10', '2', 'Вопрос №5', '0', '1', '{\"1\":\"\\u041e\\u0442\\u0432\\u0435\\u0442 \\u21161\",\"2\":\"\\u041e\\u0442\\u0432\\u0435\\u0442 \\u21162\",\"3\":\"\\u041e\\u0442\\u0432\\u0435\\u0442 \\u21163\",\"4\":\"\\u041e\\u0442\\u0432\\u0435\\u0442 \\u21164\",\"5\":\"\\u041f\\u0440\\u0430\\u0432\\u0438\\u043b\\u044c\\u043d\\u044b\\u0439 \\u043e\\u0442\\u0432\\u0435\\u0442\"}', '5', '1473367688', '1473367688');

-- ----------------------------
-- Table structure for tests
-- ----------------------------
DROP TABLE IF EXISTS `tests`;
CREATE TABLE `tests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `status` smallint(1) DEFAULT '1',
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tests
-- ----------------------------
INSERT INTO `tests` VALUES ('1', 'Тест №1', '1', '1473367688', '1473367688');
INSERT INTO `tests` VALUES ('2', 'Тест №2', '1', '1473367688', '1473367688');

-- ----------------------------
-- Table structure for test_results
-- ----------------------------
DROP TABLE IF EXISTS `test_results`;
CREATE TABLE `test_results` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `test_id` int(11) DEFAULT NULL,
  `count_answers` int(11) DEFAULT NULL,
  `count_correct_answers` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of test_results
-- ----------------------------
INSERT INTO `test_results` VALUES ('5', '1', '1', '5', '3', '1473373783', '1473373783');
INSERT INTO `test_results` VALUES ('6', '2', '1', '5', '1', '1473374036', '1473374036');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) DEFAULT '0',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `role_id` int(11) DEFAULT '2',
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `education` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`),
  KEY `user_with_user_roles` (`role_id`),
  CONSTRAINT `user_with_user_roles` FOREIGN KEY (`role_id`) REFERENCES `user_roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'OtiFLRc4LaixC9DPKjg3b2YgbVkVWzM0', '$2y$13$SQpSTvjekAMwoWrIZMwbo.breBrLU9eA7A8UytqQxiaB47s0aSNJ.', null, 'osadlhs@mail.ru', '1', '1470248523', '1473374303', '2', null, null, 'Алексей', '', '');
INSERT INTO `user` VALUES ('2', 'g5phN_5pgBfhUprg5ebKVx-k0UKOXtOS', '$2y$13$4Re4NaJMur4nYlijDHVlN.h3t1XuweZ3kGIoJoYiuYLJjC1tUIBm6', null, 'smolyak.wezom@gmail.com', '1', '1473374027', '1473374027', '2', null, '2dd4b2e7e98a887d5829fe93208d01c1.jpg', 'Смоляк Алексей', null, null);
INSERT INTO `user` VALUES ('4', 'sG4xTS9q0QofvSQzwhTLjYZEVHC-dPKm', '$2y$13$qCePMoFi36P9KntqYlc5Hurp2/GvDqVETzXks8yV965cvCL69.ycq', null, 'admin@gmail.com', '1', '1473374566', '1473374566', '1', null, null, null, null, null);

-- ----------------------------
-- Table structure for user_roles
-- ----------------------------
DROP TABLE IF EXISTS `user_roles`;
CREATE TABLE `user_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `status` smallint(6) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_roles
-- ----------------------------
INSERT INTO `user_roles` VALUES ('1', 'Администратор', '1');
INSERT INTO `user_roles` VALUES ('2', 'Пользователь', '1');
