<?php
return [
    'user.passwordResetTokenExpire' => 3600,
    
    'images' => [
        'user' => [
            [
                'folder' => 'small',
                'width' => 160,
                'height' => 160,
            ],
            
            [
                'folder' => 'big',
                'width' => 500,
                'height' => 500,
            ],
            
            [
                'folder' => 'original'
            ]
        ]
    ],
];
