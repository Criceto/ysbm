<?php
namespace common\models;

use frontend\components\Email;
use Yii;
use yii\base\Model;
use yii\db\ActiveRecord;
use yii\db\Query;

/**
 * Login form
 */
class ProfileForm extends Model
{
    public $name;
    public $email;
    public $password;
    public $education;
    public $position;
    public $id;
    public $image;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // email and password are both required
            ['email', 'required'],
            ['email', 'email'],
            ['email','validateEmail'],
            ['name', 'required'],
            ['name', 'string', 'min' => 2],
            ['password', 'string'],
            ['education', 'string'],
            ['position', 'string'],
            ['id', 'integer'],
            ['image','file','extensions' => 'png, jpg']

        ];
    }

    public function validateEmail($attribute, $params){
        if(!$this->hasErrors()){
            $user = (new Query())
                ->select([])
                ->from('user')
                ->where(['email' => $this->email])
                ->andWhere(['!=','id', $this->id])
                ->one();
            if($user){
                $this->addError($attribute,'This email is already in use by another user!');
            }
        }
    }

    public function saveData(){

        $user = User::findOne([
            'id' => $this->id
        ]);

        $user->name = $this->name;
        $user->education = $this->education;
        $user->position = $this->position;

        if($this->password){
            $user->setPassword($this->password);
        }

        return $user->save();
    }
}
