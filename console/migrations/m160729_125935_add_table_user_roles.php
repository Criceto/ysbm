<?php

use yii\db\Migration;

class m160729_125935_add_table_user_roles extends Migration
{
    public function safeUp()
    {
        $this->createTable('user_roles',array(
           'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'status' => $this->smallInteger()->defaultValue(1)
        ));

        $this->insert('user_roles',array(
           'id' => 1,
            'name' => 'Admin',
            'status' =>1
        ));

        $this->insert('user_roles',array(
            'id' => 2,
            'name' => 'User',
            'status' => 1
        ));
        
        $this->addColumn('user','role_id',$this->integer(16));

        $this->addForeignKey('user_with_user_roles','user','role_id','user_roles','id','CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('user_with_user_roles','user');
        
        $this->dropColumn('user','role_id');
        
        $this->dropTable('user_roles');
    }
    
}
