<?php

use yii\db\Migration;

class m160802_104019_change_username_in_user extends Migration
{
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->dropColumn('user','username');
        $this->addColumn('user','username', 'varchar(255) NULL');
        $this->alterColumn('user','status', $this->smallInteger()->defaultValue(0));
        $this->alterColumn('user', 'role_id', $this->integer()->defaultValue(2));
        $this->addColumn('user', 'image', $this->string(255));
        $this->addColumn('user', 'name', $this->string(255));
        $this->addColumn('user', 'position', $this->string(255));
        $this->addColumn('user', 'education', $this->string(255));
    }

    public function safeDown()
    {
        $this->alterColumn('user','username',$this->string(255));
        $this->alterColumn('user','username','NOT NULL');
        $this->alterColumn('user','status', $this->smallInteger());
        $this->alterColumn('user','status', 'NOT NULL');
        $this->alterColumn('user','role_id', $this->integer());
        $this->alterColumn('user','role_id', 'NOT NULL');
        $this->dropColumn('user','image');
        $this->dropColumn('user','name');
        $this->dropColumn('user','position');
        $this->dropColumn('user','education');
    }
}


