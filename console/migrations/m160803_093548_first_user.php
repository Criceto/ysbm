<?php

use yii\db\Migration;

class m160803_093548_first_user extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->insert('user',[
            'id' => 1,
            'auth_key' => 'OtiFLRc4LaixC9DPKjg3b2YgbVkVWzM0',
            'password_hash' => '$2y$13$SQpSTvjekAMwoWrIZMwbo.breBrLU9eA7A8UytqQxiaB47s0aSNJ.',
            'email' => 'osadlhs@mail.ru',
            'status' => 1,
            'created_at' => 1470248523,
            'updated_at' => 1470248523,
            'role_id' => 2
        ]);
    }

    public function safeDown()
    {
        $this->delete('user', ['id' => 1]);
    }

}
