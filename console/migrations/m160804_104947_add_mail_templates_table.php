<?php

use yii\db\Migration;

class m160804_104947_add_mail_templates_table extends Migration
{
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->createTable('mail_templates',[
           'id' => $this->primaryKey(),
            'name' => $this->string(255)->null(),
            'subject' => $this->string(255)->null(),
            'text' => $this->text()->null(),
            'updated_at' => $this->integer(),
            'status' => $this->smallInteger(1)->defaultValue(0),
            'sort' => $this->integer()->defaultValue(0)
        ]);

        $this->insert('mail_templates',[
            'id' => 1,
            'name' => 'Отправка нового пароля с формы "Забыли пароль"',
            'subject' => 'Новый пароль для входа на сайт {{site}}',
            'text' => '<p>Здравствуйте! Вы воспользовались формой "Забыли пароль" на сайте {{site}}. Для входа на сайт можете воспользоваться следующими данными:</p>
                        <p>&nbsp;</p>
                        <p>E-mail: {{email}}.</p>
                        <p>Пароль: {{password}}.</p>
                        <p>&nbsp;</p>
                        <p>С Ув. Администрация сайта {{site}}.</p>',
            'status' => 1,
            'sort' => 0
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('mail_templates');
    }

}
