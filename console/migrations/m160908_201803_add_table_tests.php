<?php

use yii\db\Migration;

class m160908_201803_add_table_tests extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->createTable('tests',[
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->null(),
            'status' => $this->smallInteger(1)->defaultValue(1),
            'created_at' => $this->integer(10),
            'updated_at' => $this->integer(10)
        ]);

        $this->insert('tests',[
            'name' => 'Тест №1',
            'status' => 1,
            'created_at' => time(),
            'updated_at' => time()
        ]);

        $this->insert('tests',[
            'name' => 'Тест №2',
            'status' => 1,
            'created_at' => time(),
            'updated_at' => time()
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('tests');
    }

}
