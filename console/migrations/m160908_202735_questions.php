<?php

use yii\db\Migration;

class m160908_202735_questions extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->createTable('questions',[
           'id' => $this->primaryKey(),
            'test_id' => $this->integer(),
            'name' => $this->string(255)->null(),
            'sort' => $this->integer()->defaultValue(0),
            'status' => $this->smallInteger(1)->defaultValue(1),
            'answers' => $this->text()->null(),
            'correct_answer' => $this->integer()->defaultValue(0),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);

        $this->addForeignKey('question_to_test','questions','test_id','tests','id','CASCADE','CASCADE');

        $this->insert('questions',[
            'test_id' => 1,
            'name' => 'Question №1',
            'sort' => 0,
            'status' => 1,
            'answers' => json_encode([
                1 => 'Correct answer',
                2 => 'Answer №2',
                3 => 'Answer №3',
                4 => 'Answer №4',
                5 => 'Answer №5',
            ]),
            'correct_answer' => 1,
            'created_at' => time(),
            'updated_at' => time()
        ]);

        $this->insert('questions',[
            'test_id' => 2,
            'name' => 'Question №1',
            'sort' => 0,
            'status' => 1,
            'answers' => json_encode([
                1 => 'Correct answer',
                2 => 'Answer №2',
                3 => 'Answer №3',
                4 => 'Answer №4',
                5 => 'Answer №5',
            ]),
            'correct_answer' => 1,
            'created_at' => time(),
            'updated_at' => time()
        ]);

        $this->insert('questions',[
            'test_id' => 1,
            'name' => 'Question №2',
            'sort' => 0,
            'status' => 1,
            'answers' => json_encode([
                1 => 'Answer №1',
                2 => 'Correct answer',
                3 => 'Answer №3',
                4 => 'Answer №4',
                5 => 'Answer №5',
            ]),
            'correct_answer' => 2,
            'created_at' => time(),
            'updated_at' => time()
        ]);

        $this->insert('questions',[
            'test_id' => 2,
            'name' => 'Question №2',
            'sort' => 0,
            'status' => 1,
            'answers' => json_encode([
                1 => 'Answer №1',
                2 => 'Correct answer',
                3 => 'Answer №3',
                4 => 'Answer №4',
                5 => 'Answer №5',
            ]),
            'correct_answer' => 2,
            'created_at' => time(),
            'updated_at' => time()
        ]);

        $this->insert('questions',[
            'test_id' => 1,
            'name' => 'Question №3',
            'sort' => 0,
            'status' => 1,
            'answers' => json_encode([
                1 => 'Answer №1',
                2 => 'Answer №2',
                3 => 'Correct answer',
                4 => 'Answer №4',
                5 => 'Answer №5',
            ]),
            'correct_answer' => 3,
            'created_at' => time(),
            'updated_at' => time()
        ]);

        $this->insert('questions',[
            'test_id' => 2,
            'name' => 'Question №3',
            'sort' => 0,
            'status' => 1,
            'answers' => json_encode([
                1 => 'Answer №1',
                2 => 'Answer №2',
                3 => 'Correct answer',
                4 => 'Answer №4',
                5 => 'Answer №5',
            ]),
            'correct_answer' => 3,
            'created_at' => time(),
            'updated_at' => time()
        ]);

        $this->insert('questions',[
            'test_id' => 1,
            'name' => 'Question №4',
            'sort' => 0,
            'status' => 1,
            'answers' => json_encode([
                1 => 'Answer №1',
                2 => 'Answer №2',
                3 => 'Answer №3',
                4 => 'Correct answer',
                5 => 'Answer №5',
            ]),
            'correct_answer' => 4,
            'created_at' => time(),
            'updated_at' => time()
        ]);

        $this->insert('questions',[
            'test_id' => 2,
            'name' => 'Question №4',
            'sort' => 0,
            'status' => 1,
            'answers' => json_encode([
                1 => 'Answer №1',
                2 => 'Answer №2',
                3 => 'Answer №3',
                4 => 'Correct answer',
                5 => 'Answer №5',
            ]),
            'correct_answer' => 4,
            'created_at' => time(),
            'updated_at' => time()
        ]);

        $this->insert('questions',[
            'test_id' => 1,
            'name' => 'Question №5',
            'sort' => 0,
            'status' => 1,
            'answers' => json_encode([
                1 => 'Answer №1',
                2 => 'Answer №2',
                3 => 'Answer №3',
                4 => 'Answer №4',
                5 => 'Correct answer',
            ]),
            'correct_answer' => 5,
            'created_at' => time(),
            'updated_at' => time()
        ]);

        $this->insert('questions',[
            'test_id' => 2,
            'name' => 'Question №5',
            'sort' => 0,
            'status' => 1,
            'answers' => json_encode([
                1 => 'Answer №1',
                2 => 'Answer №2',
                3 => 'Answer №3',
                4 => 'Answer №4',
                5 => 'Correct answer',
            ]),
            'correct_answer' => 5,
            'created_at' => time(),
            'updated_at' => time()
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('questions');
    }

}
