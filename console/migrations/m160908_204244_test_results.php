<?php

use yii\db\Migration;

class m160908_204244_test_results extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->createTable('test_results',[
           'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'test_id' => $this->integer(),
            'count_answers' => $this->integer(),
            'count_correct_answers' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);
    }

    public function safeDown()
    {
        $this->dropPrimaryKey('test_results');
    }

}
