<?php

use yii\db\Migration;

class m160908_223929_add_admin_user extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->insert('user',[
            'auth_key' => 'sG4xTS9q0QofvSQzwhTLjYZEVHC-dPKm',
            'password_hash' => '$2y$13$qCePMoFi36P9KntqYlc5Hurp2/GvDqVETzXks8yV965cvCL69.ycq', // пароль 123456
            'email' => 'admin@gmail.com',
            'status' => 1,
            'created_at' => time(),
            'updated_at' => time(),
            'role_id' => 1,
        ]);
    }

    public function safeDown()
    {
        $this->delete('user',['email' => 'admin@gmail.com']);
    }

}
