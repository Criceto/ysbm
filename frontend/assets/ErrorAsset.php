<?php

namespace frontend\assets;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * Main frontend application asset bundle.
 */
class ErrorAsset extends AssetBundle
{
    public $js = [
        'plugins/jQuery/jquery-2.2.3.min.js',
        'media/js/404_1.js',
        'media/js/404_2.js',
        'media/js/404_3.js',
    ];

    public $css = [
        'media/css/404_1.css',
        'media/css/404_2.css',
    ];

    public $depends = [
        'yii\web\YiiAsset', // yii.js, jquery.js
//        'yii\bootstrap\BootstrapAsset', // bootstrap.css
//        'yii\bootstrap\BootstrapPluginAsset' // bootstrap.js
    ];

    public $jsOptions = [
        'position' =>  View::POS_HEAD,
    ];
}
