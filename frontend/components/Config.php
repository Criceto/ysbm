<?php
namespace frontend\components;

use yii\base\Component;
use yii\db\Query;

class Config extends Component{
    static $_instance; // Constant that consists self class

    static function factory() {
        if(self::$_instance == NULL) { self::$_instance = new self(); }
        return self::$_instance;
    }

    public function init()
    {
        parent::init();
    }
    
    public static function get($key){
        Config::factory();

        if(\Yii::$app->params[$key]) {
            return \Yii::$app->params[$key];
        }else {
            $keys = explode('.',$key);

            $result = \Yii::$app->params;
            foreach ($keys as $obj){
                $result = $result[$obj];
            }

            return $result;
        }

        return false;
    }

}