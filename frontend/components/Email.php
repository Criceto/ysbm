<?php
namespace frontend\components;

use app\modules\admin\models\MailTemplates;
use yii\base\Component;
use yii\db\Query;

class Email extends Component{

    public function initialize()
    {
        $mail = new \PHPMailer();

        return $mail;
    }

    public static function send($mail_template_id, $variable_keys=array(), $variable_values=array(), $email_to=null, $files = []){

        if(!$email_to){
            $email_to = 'koshey08@yandex.ru';
        }

        $variable_keys += [9997 => '{{site}}', 9998 => '{{date}}', 9999 => '{{ip}}'];
        $variable_values += [9997 => $_SERVER['HTTP_HOST'], 9998 => date('d.m.Y'), 9999 => System::getRealIP()];
        
        //Получаем актуальный шаблон письма

        $template = MailTemplates::find()->where(['id' => $mail_template_id])->andWhere(['status' => 1])->one();

        if($template) {
            //заменяем все переменные, которые требуются
            $subject = str_replace($variable_keys, $variable_values, $template->subject);
            $text = str_replace($variable_keys, $variable_values, $template->text);

            $mail = self::initialize();

            $mail->setFrom('koshey08@yandex.ru', 'YSBM test');
            $mail->addReplyTo('koshey08@yandex.ru', 'YSBM test');
            $mail->Subject = $subject;
            $mail->msgHTML($text);
            $mail->addAddress($email_to);
            $mail->CharSet = 'utf-8';

            if(is_array($files) && count($files)) {
                foreach ($files as $file) {
                    if( is_file($file) ) {
                        $mail->addAttachment($file);
                    }
                }
            }
             return $mail->send();
        }

        

        return false;
        
    }
    
}