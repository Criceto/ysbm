<?php
namespace frontend\components;

use Imagine\Image\Box;
use Imagine\Image\ImageInterface;
use Imagine\Image\Point;
use yii\base\Component;
use yii\helpers\BaseFileHelper;
use yii\helpers\Url;

class Image extends Component{

    public static function upload($imageFolder, $imageName, $new_name = null){
        $sizes = Config::get('images.'.$imageFolder);
        if(!count($sizes)){
            return false;
        }

        if(!$_FILES[$imageName]){
            if(!is_file($imageName)) {
                return false;
            }
        }

        if($_FILES[$imageName]){
            $fileName = $_FILES[$imageName]['tmp_name'];
            $parts = explode('.',$_FILES[$imageName]['name']);
            $ext = $parts[count($parts)-1];
        }else{
            $fileName = $imageName;
            $parts = explode('.',$imageName);
            $ext = $parts[count($parts)-1];
        }

        if(!$new_name) {
            $new_name = md5(rand(100, 999) . time()) . '.' . $ext;
        }

        foreach ($sizes as $obj){
            BaseFileHelper::createDirectory(HOST.\Yii::getAlias('@web/media/images/'.$imageFolder.'/'.$obj['folder']),777);
            $image = \yii\imagine\Image::getImagine();
            $image=$image->open($fileName);
                if($obj['crop']){
                    if($obj['width'] && $obj['height']) {
                        $image = $image->crop(new Point(0,0),new Box($obj['width'], $obj['height']));
                    }
                } else {
                    if($obj['width'] || $obj['height']) {
                        $image = $image->thumbnail(new Box($obj['width'] ? $obj['width'] : 20000, $obj['height'] ? $obj['height'] : 20000));
                    }
                }
                $image=$image->save($_SERVER['DOCUMENT_ROOT'].\Yii::getAlias('@web/media/images/'.$imageFolder.'/'.$obj['folder']).'/'.$new_name);
        }

        return $new_name;
    }

    public static function remove($imageFolder, $fileName){
        $sizes = Config::get('images.'.$imageFolder);
        if(!count($sizes)){
            return false;
        }

        foreach ($sizes as $obj){
            if(is_file(HOST.\Yii::getAlias('@web/media/images/'.$imageFolder.'/'.$obj['folder'].'/'.$fileName))){
                unlink(HOST.\Yii::getAlias('@web/media/images/'.$imageFolder.'/'.$obj['folder'].'/'.$fileName));
            }
        }

        return true;
    }

}