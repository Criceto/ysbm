<?php
namespace frontend\components\widgets;

use frontend\components\Notify;
use yii\base\Component;
use yii\db\Query;

class Widgets extends Component{

    static $_instance; // Constant that consists self class

    public $_data = array(); // Array of called widgets
    public $_tree = array(); // Only for catalog menus on footer and header. Minus one query
    public $_menu = array(); // Only for menu

    // Instance method
    static function factory() {
        if(self::$_instance == NULL) { self::$_instance = new self(); }
        return self::$_instance;
    }

    public static function get($name, $params=array()){
        $w = Widgets::factory();
        $path = str_replace('_','/',$name);
        if(method_exists($w, $name)){
            $result = $w->{$name}($params);
            echo \Yii::$app->view->render('@app/components/widgets/views/'.$path, $result ? $result : array());
        } else {
            echo \Yii::$app->view->render('@app/components/widgets/views/'.$path, $params ? $params : array());
        }
    }

    public function Noty(){
        return Notify::getMessage();
    }

    public function Admin_LeftMenu(){

        return [
            'menu' => [],
            'count' => []
        ];
    }

    public function User_LeftMenu(){

        return [
            'menu' => [],
            'count' => []
        ];
    }

}