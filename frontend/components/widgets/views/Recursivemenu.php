<?php foreach ($menu[$active_element] as $obj){?>
    <li class="<?php echo count($menu[$obj['id']]) ? 'treeview' : '';?> <?php echo (\yii\helpers\Url::toRoute('/'.Yii::$app->controller->module->id.'/'.Yii::$app->controller->id.'/'.Yii::$app->controller->action->id) == \yii\helpers\Url::toRoute('/'.$module.'/'.$obj['link'])) ? 'active' : '';?>">
        <a href="<?php echo \yii\helpers\Url::toRoute('/'.$module.'/'.$obj['link']);?>">
            <?php if($obj['icon']){?>
                <i class="fa <?php echo $obj['icon'];?>"></i>
            <?php }?>
            <span><?php echo $obj['name'];?></span>
            <?php if(count($menu[$obj['id']]) || $count[$obj['count']]){?>
                <span class="pull-right-container">
                                    <?php if(!$count[$obj['count']]){?>
                                        <i class="fa fa-angle-left pull-right"></i>
                                    <?php } else {?>
                                        <span class="label label-primary pull-right"><?php echo $count[$obj['count']];?></span>
                                    <?php }?>
                                </span>
            <?php }?>
        </a>
        <?php if(count($menu[$obj['id']])){?>
            <ul class="treeview-menu">
                <?php echo \frontend\components\widgets\Widgets::get('RecursiveMenu',
                    [
                        'menu' => $menu,
                        'count' => $count,
                        'module' => $module,
                        'active_element' => $obj['id']
                    ]
                );?>
            </ul>
        <?php }?>
    </li>
<?php }?>
