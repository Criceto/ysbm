<!-- About Me Box -->
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">About me</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <strong><i class="fa fa-book margin-r-5"></i> Education</strong>

        <p class="text-muted education-text">
            <?php echo $user->education ? $user->education : 'Not indicated';?>
        </p>

        <hr>

        <strong><i class="fa fa-map-marker margin-r-5"></i> Position</strong>

        <p class="text-muted position-text"><?php echo $user->position ? $user->position : 'Developer';?></p>

        <hr>

        <strong><i class="fa fa-pencil margin-r-5"></i> Skills</strong>

        <p>
            <span class="label label-danger">UI Design</span>
            <span class="label label-success">Coding</span>
            <span class="label label-info">Javascript</span>
            <span class="label label-warning">PHP</span>
            <span class="label label-primary">Node.js</span>
        </p>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->