<div class="col-md-9">
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#settings" data-toggle="tab">Settings</a></li>
        </ul>
        <div class="tab-content">
            <div class="active tab-pane" id="settings">
                <?
                $form = \yii\bootstrap\ActiveForm::begin([
                    'enableClientValidation' => false,
                    'enableAjaxValidation' => true,
                    'action' => '/user/user/profile-post',
                    'options' => [
                        'class' => 'form-horizontal wForm',
                        'enctype' => 'multipart/form-data'
                    ]
                ]);?>

                <div class="">
                    <label for="inputName" class="col-sm-2 control-label">Name</label>
                    <div class="col-sm-10">
                        <?php echo $form->field($profile_model,'name')->textInput([
                            'class' => 'form-control',
                            'value' => $user->name,
                            'placeholder' => 'Имя',
                            'type' => 'text'
                        ])->label(false);?>
                    </div>
                </div>
                <div class="">
                    <label for="inputEmail" class="col-sm-2 control-label">E-mail</label>
                    <div class="col-sm-10">
                        <?php echo $form->field($profile_model,'email')->textInput([
                            'class' => 'form-control',
                            'value' => $user->email,
                            'placeholder' => 'E-mail',
                            'type' => 'email'
                        ])->label(false);?>
                    </div>
                </div>
                <div class="">
                    <label for="inputName" class="col-sm-2 control-label">Password</label>

                    <div class="col-sm-10">
                        <?php echo $form->field($profile_model,'password')->passwordInput([
                            'class' => 'form-control',
                            'placeholder' => 'Password',
                            'data-toggle' => 'tooltip',
                            'title' => 'Leave blank if you do not want to change your password'
                        ])->label(false);?>
                    </div>
                </div>
                <div class="">
                    <label for="inputExperience" class="col-sm-2 control-label">Education</label>

                    <div class="col-sm-10">
                        <?php echo $form->field($profile_model, 'education')->textarea([
                            'class' => 'form-control',
                            'placeholder' => 'Education',
                            'value' => $user->education
                        ])->label(false);?>
                    </div>
                </div>
                <div class="">
                    <label for="inputSkills" class="col-sm-2 control-label">Position</label>

                    <div class="col-sm-10">
                        <?php echo $form->field($profile_model,'position')->textInput([
                            'class' => 'form-control',
                            'value' => $user->position,
                            'placeholder' => 'Position',
                            'type' => 'text'
                        ])->label(false);?>
                    </div>
                </div>
                <div class="">
                    <label for="inputSkills" class="col-sm-2 control-label">Image</label>

                    <div class="col-sm-10">
                        <div class="form-group">
                            <?php echo \frontend\components\widgets\Widgets::get('Dropzone', [
                                'name' => 'image',
                                'uploadUrl' => \yii\helpers\Url::to('/user/user/upload-photo'),
                                'maxFiles' => 1,
                                'removeUrl' => \yii\helpers\Url::to('/user/user/remove-image'),
                                'sending' => 'function(file, xhr_o, data){
                                                   data.append("user_id", '.$user->id.')
                                                }',
                            ]);?>
                        </div>
                        <?php if(is_file(HOST.Yii::getAlias('@web/media/images/user/small/'.$user->image))){?>
                            <div class="dropzone-preview"><?php echo json_encode([
                                    [
                                        'name' => $user->image,
                                        'size' => filesize(HOST.Yii::getAlias('@web/media/images/user/small/'.$user->image)),
                                        'path' => Yii::getAlias('@web/media/images/user/small/'.$user->image)
                                    ]
                                ]);?>
                            </div>
                        <?php }?>
                    </div>
                </div>
                <?php echo $form->field($profile_model, 'id')->hiddenInput([
                    'value' => $user->id
                ])->label(false);?>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <?php echo \yii\helpers\Html::submitButton('Send',['class' => 'btn btn-danger']);?>
                    </div>
                </div>
                <?php \yii\bootstrap\ActiveForm::end(); ?>
            </div>
            <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->
    </div>
    <!-- /.nav-tabs-custom -->
</div>