<!-- Profile Image -->
<div class="box box-primary">
    <div class="box-body box-profile">
        <?php if(is_file(HOST.\Yii::getAlias('@web/media/images/user/small/'.$user->image))){?>
            <img class="profile-user-img img-responsive user-avatar img-circle" src="<?php echo \yii\helpers\Url::to('@web/media/images/user/small/'.$user->image, true);?>" alt="User profile picture">
        <?php } else {?>
            <img class="profile-user-img img-responsive user-avatar img-circle" src="<?php echo \yii\helpers\Url::to('@web/media/pic/avatar.png');?>" alt="User profile picture">
        <?php }?>

        <h3 class="profile-username text-center user-name"><?php echo $user->name ? $user->name : $user->email;?></h3>
        <p class="text-muted text-center position-text"><?php echo $user->position ? $user->position : 'Программист';?></p>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->