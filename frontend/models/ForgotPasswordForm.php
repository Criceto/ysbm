<?php
namespace frontend\models;

use frontend\components\Email;
use Yii;
use yii\base\Model;
use common\models\User;

/**
 * Password reset request form
 */
class ForgotPasswordForm extends Model
{
    public $email;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required',],
            ['email', 'email',],
            ['email', 'exist',
                'targetClass' => '\common\models\User',
                'filter' => ['status' => User::STATUS_ACTIVE],
                'message' => 'User with this email does not exist!'
            ],
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return boolean whether the email was send
     */
    public function sendEmail()
    {
        /* @var $user User */
        $user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'email' => $this->email,
        ]);

        if (!$user) {
            return false;
        }
        
        $new_password = rand(10000000,99999999);
        $user->setPassword($new_password);
        
        if (!$user->save()) {
            return false;
        }

        return Email::send(1,['{{password}}', '{{email}}'], [$new_password,$this->email], $this->email);
    }
}
