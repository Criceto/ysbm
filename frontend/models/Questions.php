<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "questions".
 *
 * @property integer $id
 * @property integer $test_id
 * @property string $name
 * @property integer $sort
 * @property integer $status
 * @property string $answers
 * @property integer $correct_answer
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Tests $test
 */
class Questions extends \yii\db\ActiveRecord
{

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'questions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['test_id', 'sort', 'status', 'correct_answer', 'created_at', 'updated_at'], 'integer'],
            [['answers'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['test_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tests::className(), 'targetAttribute' => ['test_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'test_id' => 'Test ID',
            'name' => 'Name',
            'sort' => 'Sort',
            'status' => 'Status',
            'answers' => 'Answers',
            'correct_answer' => 'Correct Answer',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTest()
    {
        return $this->hasOne(Tests::className(), ['id' => 'test_id']);
    }

    public static function getQuestionsByTest($test_id){
        return self::find()->where(['test_id' => $test_id])->orderBy(['sort' => SORT_ASC])->all();
    }
}
