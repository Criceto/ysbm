<?php

namespace app\models;

use common\models\User;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "test_results".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $test_id
 * @property integer $count_answers
 * @property integer $count_correct_answers
 * @property integer $created_at
 * @property integer $updated_at
 */
class TestResults extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'test_results';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'test_id', 'count_answers', 'count_correct_answers', 'created_at', 'updated_at'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'test_id' => 'Test ID',
            'count_answers' => 'Count Answers',
            'count_correct_answers' => 'Count Correct Answers',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    static public function getResults($test_id){
        return self::find()->joinWith('user')->where(['test_id' => $test_id])->orderBy('created_at', 'DESC')->all();
    }
}
