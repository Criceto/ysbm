<?php

namespace app\modules\admin;
use yii\helpers\Url;

/**
 * admin module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\admin\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->setLayoutPath('@frontend/views/layouts');

        if(\Yii::$app->user->identity->role_id !== 1){
            \Yii::$app->response->redirect(Url::home(),301);
        }

        parent::init();

        // custom initialization code goes here
    }
}
