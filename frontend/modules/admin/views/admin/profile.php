<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Personal profile
        </h1>
        <?php echo \frontend\components\Breadcrumbs::generateBreadcrumbs();?>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">
            <div class="col-md-3">

                <?php echo \frontend\components\widgets\Widgets::get('Admin_Profile_Profile', ['user' => $user]);?>

                <?php echo \frontend\components\widgets\Widgets::get('User_Profile_About');?>
            </div>
            <!-- /.col -->
            <?php echo \frontend\components\widgets\Widgets::get('User_Profile_Form', [
                'profile_model' => $profile_model,
                'user' => $user
            ]);?>
            <!-- /.col -->
        </div>
        <!-- /.row -->

    </section>
    <!-- /.content -->
</div>