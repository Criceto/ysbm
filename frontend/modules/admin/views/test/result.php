<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Dashboard
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">List of tests</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-bordered">
                            <?php if(count($result)){?>
                                <tr>
                                    <th style="width: 10px">#</th>
                                    <th>Username</th>
                                    <th>Result</th>
                                </tr>
                                <?php foreach ($result as $key=>$obj){?>
                                    <tr>
                                        <td><?php echo $k+1;?></td>
                                        <td><?php echo $obj->user->name ? $obj->user->name : $obj->user->email;?></td>
                                        <td>
                                            <span><?php echo $obj->count_correct_answers.'/'.$obj->count_answers;?></span>
                                            <?php $percent =($obj->count_correct_answers/$obj->count_answers)*100?>
                                            <div class="progress progress-xs">
                                                <div class="progress-bar progress-bar-<?php echo $percent>50 ? 'success' : 'danger';?>" style="width: <?php echo (int)$percent?>%"></div>
                                            </div>
                                        </td>                                    </tr>
                                <?php }?>
                            <?php } else {?>
                                <h2>Not yet test resultst</h2>
                            <?php }?>
                        </table>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer clearfix">
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>

    </section>
    <!-- /.content -->
</div>