<?php

namespace app\modules\user\controllers;

use app\models\TestResults;
use app\models\Tests;
use common\models\LoginForm;
use common\models\User;
use common\widgets\Alert;
use frontend\components\Config;
use frontend\components\Email;
use frontend\components\Notify;
use frontend\controllers\BaseController;
use frontend\models\ForgotPasswordForm;
use frontend\models\RegistrationForm;
use yii\bootstrap\Html;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Response;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * Default controller for the `user` module
 */
class IndexController extends BaseController
{

    public function beforeAction($action)
    {
        $this->layout = 'user';
        return parent::beforeAction($action);
    }

    public function actionIndex(){

        $this->view->title = 'Tests list';

        $result = TestResults::find()->where(['user_id'=>\Yii::$app->user->identity->id])->all();

        $test_ids = [];
        foreach ($result as $obj){
            $test_ids[] = $obj->test_id;
        }

        $dataProvider = new ActiveDataProvider([
            'query' => Tests::find()->where(['NOT IN','id',$test_ids])->andWhere(['status' => 1])
        ]);

        $dataProvider->setPagination(false);

        return $this->render('index',[
            'dataProvider' => $dataProvider
        ]);
    }
    
}
