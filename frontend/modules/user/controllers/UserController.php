<?php

namespace app\modules\user\controllers;

use common\models\LoginForm;
use common\models\ProfileForm;
use common\models\User;
use common\widgets\Alert;
use frontend\components\Breadcrumbs;
use frontend\components\Config;
use frontend\components\Email;
use frontend\components\Image;
use frontend\components\Notify;
use frontend\controllers\BaseController;
use frontend\models\ForgotPasswordForm;
use frontend\models\RegistrationForm;
use rmrevin\yii\ulogin\AuthAction;
use rmrevin\yii\ulogin\ULogin;
use yii\bootstrap\Html;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\BaseFileHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * Default controller for the `user` module
 */
class UserController extends BaseController
{

    public function beforeAction($action)
    {
        Breadcrumbs::setBread('Home', Url::toRoute('/user/index/index'), ' <li><a href="{{link}}"><i class="fa fa-dashboard"></i> {{name}}</a></li>');
        $this->enableCsrfValidation = ($action->id !== "ulogin");
        return parent::beforeAction($action);
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionLogin()
    {
        $this->layout = 'login';

        \Yii::$app->view->title = 'YSBM test';
        
        $register_model = new RegistrationForm();
        $login_model = new LoginForm();
        $forgot_model = new ForgotPasswordForm();

        return $this->render('login',array(
            'register_model' => $register_model,
            'login_model' => $login_model,
            'forgot_model' => $forgot_model
        ));
    }
    
    public function actionLogout(){
        \Yii::$app->user->logout();

        Notify::setMessage('success', 'Come back again!');
        
        $this->redirect(Url::home(),301);
    }

    public function actionProfile(){
        $this->layout = 'user';

        $this->view->title = 'Personal profile';
        
        Breadcrumbs::setBread('Personal profile');

        $profileModel = new ProfileForm();

        return $this->render('profile',array(
            'user' => \Yii::$app->user->identity,
            'profile_model' => $profileModel
        ));
    }

    public function actionRegistrationPost(){
        $register_model = new RegistrationForm();
        if(\Yii::$app->request->isAjax && \Yii::$app->request->isPost){
            if ($register_model->load(\Yii::$app->request->post())) {
                \Yii::$app->response->format = Response::FORMAT_JSON;
                $valid = ActiveForm::validate($register_model);
                if(!count($valid) && !\Yii::$app->request->post('ajax')){
                    $result = $register_model->regist();
                    if($result) {
                        \Yii::$app->user->login($result);
                        Notify::setMessage('success', 'Thank you, you have successfully logged in!');
                        $this->success([
                            'jquery' => [
                                [
                                    'action' => 'redirect',
                                    'link' => Url::toRoute(['/user/index/index'])
                                ]
                            ]
                        ]);
                    }
                } else {
                    return $valid;
                }
            }
        }
    }

    public function actionLoginPost(){
        $login_model = new LoginForm();
        if(\Yii::$app->request->isAjax && \Yii::$app->request->isPost){
            if ($login_model->load(\Yii::$app->request->post())) {
                \Yii::$app->response->format = Response::FORMAT_JSON;
                $valid = ActiveForm::validate($login_model);
                if(!count($valid) && !\Yii::$app->request->post('ajax')){
                    //проверяем, залогинило ли пользователя
                    if($login_model->login()){
                        Notify::setMessage('success', 'Welcome to the site!');
                        $redirect = '';
                        if(\Yii::$app->user->identity->role_id == 2){
                            $redirect = Url::toRoute(['/user/index/index']);
                        } elseif(\Yii::$app->user->identity->role_id === 1){
                            $redirect = Url::toRoute('/admin/index/index');
                        }
                        $this->success([
                            'jquery' => [
                                [
                                    'action' => 'redirect',
                                    'link' => $redirect
                                ]
                            ]
                        ]);
                    }else {
                        $this->error('Incorrect login or password!');
                    }
                } else {
                    return $valid;
                }
            }
        }
    }

    public function actionForgotPost(){
        $forgot_model = new ForgotPasswordForm();
        if(\Yii::$app->request->isAjax && \Yii::$app->request->isPost){
            if ($forgot_model->load(\Yii::$app->request->post())) {
                \Yii::$app->response->format = Response::FORMAT_JSON;
                $valid = ActiveForm::validate($forgot_model);
                if(!count($valid) && !\Yii::$app->request->post('ajax')){
                    $result = $forgot_model->sendEmail();
                    if($result) {
                        $this->success('New password sent to your e-mail!');
                    } else {
                        $this->error('Failed to send email, check mail settings!');
                    }
                } else {
                    return $valid;
                }
            }
        }
    }

    public function actionProfilePost(){
        $profile_form = new ProfileForm();
        if(\Yii::$app->request->isAjax && \Yii::$app->request->isPost){
            if ($profile_form->load(\Yii::$app->request->post())) {
                \Yii::$app->response->format = Response::FORMAT_JSON;
                $valid = ActiveForm::validate($profile_form);
                if(!count($valid) && !\Yii::$app->request->post('ajax')){
                    $result = $profile_form->saveData();
                    if($result) {
                        $this->success([
                            'jquery' => [
                                [
                                    'action' => 'message',
                                    'text' => 'New data has been successfully saved!',
                                    'type' => 'success'
                                ],
                                [
                                    'action' => 'insertion',
                                    'selector' => '.user-name',
                                    'content' => $profile_form->name ? $profile_form->name : $profile_form->email,
                                    'method' => 'text',
                                ],
                                [
                                    'action' => 'insertion',
                                    'selector' => '.education-text',
                                    'content' => $profile_form->education,
                                    'method' => 'text',
                                ],
                                [
                                    'action' => 'insertion',
                                    'selector' => '.position-text',
                                    'content' => $profile_form->position ? $profile_form->position : 'Developer',
                                    'method' => 'text',
                                ],
                            ]
                        ]);
                    } else {
                        $this->error('Something went wrong, reload the page and try again!');
                    }
                } else {
                    return $valid;
                }
            }
        }
    }

    public function actionUploadPhoto(){
        if(\Yii::$app->request->isPost){
            $user_id = \Yii::$app->request->post('user_id');

            $name = Image::upload('user','image');

            if($name){
                $user = User::findOne(['id' => $user_id]);
                $user->image = $name;
                $user->save();
            }

            $this->success([
                'name' => $name,
                'jquery' => [
                    [
                        'action' => 'attributes',
                        'attribute' => 'src',
                        'selector' => '.user-avatar',
                        'value' => Url::to('@web/media/images/user/small/'.$name,true),
                        'method' => 'attr'
                    ]
                ],
            ]);

        }
    }

    public function actionRemoveImage(){
        if(\Yii::$app->request->isPost){
            $file_name = \Yii::$app->request->post('name');

            $result = Image::remove('user', $file_name);

            if($result){
                $user = User::findOne(['image' => $file_name]);
                $user->image = '';
                $user->save();

                $this->success([
                    'jquery' => [
                        [
                            'action' => 'attributes',
                            'attribute' => 'src',
                            'selector' => '.user-avatar',
                            'value' => Url::to('@web/media/pic/avatar.png',true),
                            'method' => 'attr'
                        ]
                    ],
                ]);
            }

        }
    }

    public function actionUlogin(){
        if(\Yii::$app->request->post('token')) {
            $data = ULogin::getUserAttributes(\Yii::$app->request->post('token'));
            if(!$data['error']){
                $user = User::findOne(['email' => $data['email']]);

                //если пользователь существует, авторизуем его
                if($user){
                    \Yii::$app->user->login($user, time()+60*60*24*3);
                } else {
                    
                    $new_password = rand(1000000,9999999);

                    $image = '';
                    if($data['photo_big']) {
                        //получаем изображение
                        $file = file_get_contents($data['photo_big']);
                        $im = '';
                        if ($file) {
                            $parts = explode('.', $data['photo_big']);
                            $ext = $parts[count($parts) - 1];

                            $im = md5(rand(100, 999) . time()) . '.' . $ext;
                            file_put_contents(HOST . \Yii::getAlias('@web/media/images/user/' . $im), $file);
                        }

                        $image = Image::upload('user', HOST.\Yii::getAlias('@web/media/images/user/' . $im), $im);
                    }
                    
                    //Регистрируем нового пользователя в системе
                    $user = new User();
                    $user->email = $data['email'];
                    $user->name = $data['last_name'].' '.$data['first_name'];
                    $user->setPassword($new_password);
                    $user->generateAuthKey();
                    $user->image = $image ? $image : '';
                    $user->role_id = 2;
                    $user->save();

                    Email::send(1,['{{password}}', '{{email}}'], [$new_password,$user->email], $user->email);

                    unlink(HOST.\Yii::getAlias('@web/media/images/user/' . $im));

                    \Yii::$app->user->login($user);
                }

                Notify::setMessage('success', 'Welcome to the site!');

                //проверяем, куда редиректить данного пользователя
                if($user->role_id == 2) {
                    $this->redirect(Url::toRoute('/user/index/index'), 301);
                }
            } else {
                Notify::setMessage('error', 'Something went wrong, try again later');
                $this->redirect(Url::home(),301);
            }
        }
    }
    
}
