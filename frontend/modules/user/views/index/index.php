<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Dashboard
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">List of tests</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-bordered">
                            <?php if($dataProvider->getTotalCount()){?>
                                <tr>
                                    <th style="width: 10px">#</th>
                                    <th>Name</th>
                                    <th style="width: 100px">Start</th>
                                </tr>
                                <?php foreach ($dataProvider->getModels() as $key=>$obj){?>
                                    <tr>
                                        <td><?php echo $k+1;?></td>
                                        <td><?php echo $obj->name;?></td>
                                        <td><a href="<?php echo \yii\helpers\Url::to('/user/test/'.$obj->id);?>" class="btn btn-block btn-success">Start</a></td>
                                    </tr>
                                <?php }?>
                            <?php } else {?>
                                <h2>has not added any test</h2>
                            <?php }?>
                        </table>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer clearfix">
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>

    </section>
    <!-- /.content -->
</div>