<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h2>
            <?php echo Yii::$app->view->title;?>
        </h2>
        <?php echo \frontend\components\Breadcrumbs::generateBreadcrumbs();?>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <?php
                $form = \yii\bootstrap\ActiveForm::begin([
                    'action' => '/user/test/post/'.$id,
                    'options' => [
                        'class' => 'wForm',
                        'enctype' => 'multipart/form-data'
                    ]
                ]);?>
               <div class="row">
                   <div class="col-md-12">
                       <?php foreach ($questions as $obj){?>
                           <div class="form-group list-questions">
                               <label for="question_<?php echo $obj->id?>" class="control-label"><?php echo $obj->name;?></label>
                                   <?php echo \yii\helpers\Html::radioList('question['.$obj->id.']','',json_decode($obj->answers),[
                                       'id' => 'question_'.$obj->id,
                                       'class' => 'form-control '
                                   ]);?>
                           </div>
                       <?php }?>
                       <?php echo \yii\helpers\Html::button('Send',[
                           'type' => 'submit',
                           'class' => 'wSubmit btn btn-block btn-success'
                       ]);?>
                   </div>
               </div>
                <?php \yii\bootstrap\ActiveForm::end(); ?>
                <!-- /.nav-tabs-custom -->
            </div>
        </div>
    </section>
</div>