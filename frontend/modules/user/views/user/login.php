<div class="login_page">
    <div class="login-box">
        <div class="login-logo">
            <a href="../../index2.html"><b>YSBM</b> test</a>
        </div>
               
        <div class="login-box-body login-form">
            <p class="login-box-msg">Log in to use the system</p>
            <?
            $form = \yii\bootstrap\ActiveForm::begin([
                'enableClientValidation' => false,
                'enableAjaxValidation' => true,
                'action' => 'user/user/login-post',
                'options' => [
                    'class' => 'wForm'
                ]
            ]);?>
                <div class="form-group has-feedback">
                    <?php echo $form->field($login_model, 'email')->textInput([
                        'class' => 'form-control',
                        'placeholder' => 'E-mail',
                        'type' => 'email'
                    ])->label(false);?>
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <?php echo $form->field($login_model, 'password')->passwordInput([
                        'class' => 'form-control',
                        'placeholder' => 'Password',
                    ])->label(false);?>
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="row">
                    <div class="col-xs-8">
                        <div class="checkbox icheck">
                            <label>
                                <?php echo $form->field($login_model,'rememberMe')->checkbox()->label('Remember me');?>
                            </label>
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-xs-4">
                        <?php echo \yii\bootstrap\Html::submitButton('Log in',[
                            'class' => 'btn btn-primary btn-block btn-flat'
                        ]);?>
                    </div>
                    <!-- /.col -->
                </div>
            <?php \yii\bootstrap\ActiveForm::end(); ?>

            <div class="social-auth-links text-center">
                <div class="ulogin">
                    <?php
                    echo \rmrevin\yii\ulogin\ULogin::widget([
                        // widget look'n'feel
                        'display' => \rmrevin\yii\ulogin\ULogin::D_PANEL,
    
                        // required fields
                        'fields' => [\rmrevin\yii\ulogin\ULogin::F_FIRST_NAME, \rmrevin\yii\ulogin\ULogin::F_LAST_NAME, \rmrevin\yii\ulogin\ULogin::F_EMAIL, \rmrevin\yii\ulogin\ULogin::F_PHONE, \rmrevin\yii\ulogin\ULogin::F_CITY, \rmrevin\yii\ulogin\ULogin::F_COUNTRY, \rmrevin\yii\ulogin\ULogin::F_PHOTO_BIG],
    
                        // optional fields
                        'optional' => [\rmrevin\yii\ulogin\ULogin::F_BDATE],
    
                        // login providers
                        'providers' => [\rmrevin\yii\ulogin\ULogin::P_VKONTAKTE, \rmrevin\yii\ulogin\ULogin::P_GOOGLE],
    
                        // login providers that are shown when user clicks on additonal providers button
                        'hidden' => [],
    
                        // where to should ULogin redirect users after successful login
                        'redirectUri' => ['user/user/ulogin'],
    
                        // optional params (can be ommited)
                        // force widget language (autodetect by default)
                        'language' => \rmrevin\yii\ulogin\ULogin::L_RU,
    
                        // providers sorting ('relevant' by default)
                        'sortProviders' => \rmrevin\yii\ulogin\ULogin::S_RELEVANT,
    
                        // verify users' email (disabled by default)
                        'verifyEmail' => '0',
    
                        // mobile buttons style (enabled by default)
                        'mobileButtons' => '1',
                    ]);
                    ?>
                </div>
                <p>- ИЛИ -</p>
                <a href="#" class="btn btn-block btn-social btn-facebook btn-flat btn-vk"><i class="fa fa-vk"></i> Log in with
                    VK</a>
                <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Log in with
                    Google+</a>
            </div>
            <!-- /.social-auth-links -->

            <a href="#" class="forgot-password-but">Forgot password?</a><br>
            <a href="#" class="text-center reg-but">Registration</a>

        </div>

        <div class="login-box-body registration-form" style="display: none">
            <p class="login-box-msg">New User Registration</p>

            <?
            $form = \yii\bootstrap\ActiveForm::begin([
                'enableClientValidation' => false,
                'enableAjaxValidation' => true,
                'action' => 'user/user/registration-post',
                'options' => [
                    'class' => 'wForm'
                ]
            ]);
            ?>
                <div class="form-group has-feedback">
                    <?php echo $form->field($register_model, 'email')->textInput([
                        'class' => 'form-control',
                        'placeholder' => 'E-mail',
                        'type' => 'email'
                    ])->label(false);?>
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <?php echo $form->field($register_model, 'password')->passwordInput([
                        'class' => 'form-control',
                        'placeholder' => 'Password',
                    ])->label(false);?>
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="row">
                    <div class="col-xs-6">
                    </div>
                    <!-- /.col -->
                    <div class="col-xs-6">
                        <?php echo \yii\helpers\Html::submitButton('Registration',['class' => 'btn btn-primary btn-block btn-flat']);?>
                    </div>
                    <!-- /.col -->
                </div>
            <?php \yii\bootstrap\ActiveForm::end(); ?>

            <a href="#" class="login-page-back">Return</a><br>

        </div>

        <div class="login-box-body forgot-form" style="display: none">
            <p class="login-box-msg">Forgot password?</p>

            <?
            $form = \yii\bootstrap\ActiveForm::begin([
                'enableClientValidation' => false,
                'enableAjaxValidation' => true,
                'action' => 'user/user/forgot-post',
                'options' => [
                    'class' => 'wForm'
                ]
            ]);
            ?>
                <div class="form-group has-feedback">
                    <?php echo $form->field($forgot_model, 'email')->textInput([
                        'class' => 'form-control',
                        'placeholder' => 'E-mail',
                        'type' => 'email'
                    ])->label(false);?>
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
                <div class="row">
                    <div class="col-xs-7">
                    </div>
                    <!-- /.col -->
                    <div class="col-xs-5">
                        <?php echo \yii\helpers\Html::submitButton('SEND', ['class' => 'btn btn-primary btn-block btn-flat']);?>
                    </div>
                    <!-- /.col -->
                </div>
            <?php \yii\bootstrap\ActiveForm::end(); ?>

            <a href="#" class="login-page-back">Return</a><br>

        </div>
        <!-- /.login-box-body -->
    </div>
    <!-- /.login-box -->
</div>

<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    });
</script>