var generate = function( message, type, time ) {
    var timeout = time;
    if(!timeout){
        timeout = 3000;
    }
    var n = noty({
        text: message,
        type: type,
        animation: {
            open: 'animated flipInY', // Animate.css class names
            close: 'animated flipOutY' // Animate.css class names
        },
        speed: 500,
        timeout: timeout
    });
};

var parseAjaxAnswer = function(data) {
    if(data.success){
        if(data.response){
            generate(data.response, 'success');
        }
    } else {
        if(data.response){
            generate(data.response,'error');
        }
    }

    if(data.redirect){
        window.location.href = data.redirect;
    }

    if(data.replace_text){
        var arr = data.replace_text;
        $.each(arr, function (key, value) {
            $(key).text(arr[key]);
        });
    }

    if(data.change_attr){
        $.each(data.change_attr, function (key, obj) {
                $(obj.selector).attr(obj.attr,obj.value);
        })
    }
};


$(function () {
    
    $('.reg-but').on('click',function (e) {
        e.preventDefault();
        $(this).closest('.login-form').slideUp(500, function () {
            $('.registration-form').slideDown(500);
        });
    });

    $('.login-page-back').on('click', function (e) {
        e.preventDefault();
        $(this).closest('.login-box-body').slideUp(500, function () {
            $('.login-form').slideDown(500);
        });
    });

    $('.forgot-password-but').on('click',function (e) {
        e.preventDefault();
        $(this).closest('.login-form').slideUp(500, function () {
            $('.forgot-form').slideDown(500);
        });
    });

    $('.wForm').on('beforeSubmit',function (event) {
        event.preventDefault();
        var mark = '<div class="cssload-body"><span><span></span><span></span><span></span><span></span></span><div class="cssload-base"><span></span><div class="cssload-face"></div></div></div><div class="cssload-longfazers"><span></span><span></span><span></span><span></span></div>';
        Preloader.show(false, {
            block: true,
            markup: mark
        });
        var action = $(this).attr('action');
        var form = $(this);
        var data = form.serialize();
        $.ajax({
            url: action,
            type: 'POST',
            dataType: 'JSON',
            data: data,
            success: function(data){
                Preloader.hide();
                wAjax.start(data.jquery, $('body'));
            }
        });
        return false;
    });

    $(document).ready(function () {

        if($('.dropzone-preview').length) {

            var previews = JSON.parse($('.dropzone-preview').text());

            $.each(previews, function (key, obj) {
                // console.log(obj);
                // Create the mock file:
                var mockFile = {name: obj.name, size: obj.size};

                // Call the default addedfile event handler
                $('#myDropzone').get(0).dropzone.emit("addedfile", mockFile);

                // And optionally show the thumbnail of the file:
                $('#myDropzone').get(0).dropzone.emit("thumbnail", mockFile, obj.path);
                // Or if the file on your server is not yet in the right
                // size, you can let Dropzone download and resize it
                // callback and crossOrigin are optional.
                $('#myDropzone').get(0).dropzone.createThumbnailFromUrl(mockFile, obj.path);

                // Make sure that there is no progress bar, etc...
                $('#myDropzone').get(0).dropzone.emit("complete", mockFile);
            });
        }
    });

    //#костыль по авторизации через соц сети
    $('.btn-facebook').on('click', function (event) {
        event.preventDefault();
        $('.ulogin-button-vkontakte').trigger('click')
    });

    $('.btn-google').on('click', function (event) {
        event.preventDefault();
        $('.ulogin-button-google').trigger('click')
    });
    //---------------------------------------------------------------------------------

    //рекурсивное добавление актиных класов для левоо меню
    var activep = $('body').find('.sidebar-menu').find('.active');
    function recursiveActive(activePunkt) {
        if(activePunkt.closest('.sidebar-menu').length){
            activePunkt.addClass('active');
            if(activePunkt.parent().closest('.treeview').length){
                return recursiveActive(activePunkt.parent().closest('.treeview'));
            }
            return true;
        }

        return true;
    }
    recursiveActive(activep);
    //---------------------------------------------------------------------------------------

    //кнопка просмотра пароля на странице настроек сайта
    $('.show-password').mousedown(function(){
        $(this).closest('.input-group').find('.password-field').attr('type','text');
    });
    $('.show-password').mouseup(function(){
        $(this).closest('.input-group').find('.password-field').attr('type','password');
    });
    //--------------------------------------------------------------------------------------------
    
    
    //инициализация тиньки
    if($('.tinymceEditor').length){
        tinymce.init({
            selector: "textarea.tinymceEditor",
            skin : "wezom",
            language : 'ru',
            plugins: [
                "advlist autolink lists link image charmap print preview hr",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons paste textcolor colorpicker textpattern responsivefilemanager"
            ],
            table_class_list: [
                {title: 'Поумолчанию', value: ''},
                {title: 'Без границ', value: 'table-null'},
                {title: 'Зебра', value: 'table-zebra'}
            ],
            image_class_list: [
                {title: 'Поумолчанию', value: ''}
            ],
            toolbar1: "undo redo pastetext | bold italic forecolor backcolor fontselect fontsizeselect styleselect | alignleft aligncenter alignright alignjustify",
            toolbar2: 'bullist numlist outdent indent | link unlink image responsivefilemanager fullscreen',
            image_advtab: true,
            external_filemanager_path:"/media/js/tinymce/filemanager/",
            filemanager_title:"Менеджер файлов" ,
            external_plugins: { "filemanager" : "filemanager/plugin.min.js"},
            document_base_url: "http://"+window.location.hostname+"/",
            convert_urls: false,
            plugin_preview_width: "1000",
            relative_urls: false,
            default_language:'ru',
            fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt"
        });

        var wLastSmall, wLastBig;
        if($(window).width() > 700){
            wLastSmall = false;
            wLastBig = true;
        }else{
            wLastSmall = true;
            wLastBig = false;
        }

        $(window).on('resize',function(){

            if($(window).width() > 700 && wLastSmall){
                wLastSmall = false;
                wLastBig = true;
                parent.tinyMCE.activeEditor.windowManager.close(window);
            }
            if($(window).width() < 700 && wLastBig){
                wLastSmall = true;
                wLastBig = false;
                parent.tinyMCE.activeEditor.windowManager.close(window);
            }
        })
    }
    //---------------------------------------------------------------------------------------------


});